﻿using System;

namespace ProjectEuler
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            try
            {
                Console.Write("Problem#: ");
                var problemNumber = Convert.ToInt32(Console.ReadLine());

                var answer = Activator.CreateInstance(Type.GetType(string.Format("ProjectEuler.Answer{0}", problemNumber.ToString("0000"))));

                Console.WriteLine("Answer{0} {1}", problemNumber, answer);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine("====================================================");
                Console.WriteLine("Usage: ProjectEuler <problem#>");
            }
            Console.ReadLine();
        }
    }
}








﻿using System;

namespace ProjectEuler
{
    /// <summary>
    /// Find the difference between the sum of the squares of the first
    /// one hundred natural numbers and the square of the sum.
    /// </summary>
    public class Answer0006
    {
        public override string ToString()
        {
            int n = 100;
            long squareOfSums = 0;
            long sumOfSquares = 0;
            for (int i = 0; i <= n; i++)
            {
                squareOfSums += i;
                
                sumOfSquares += i * i;
            }
            squareOfSums = squareOfSums * squareOfSums;
            
            return (squareOfSums - sumOfSquares).ToString();
        }
    }
}


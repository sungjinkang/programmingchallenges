﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectEuler
{
    public class Answer0017
    {
        static Dictionary<string, string> intToWord = new Dictionary<string, string>()
        {
            { "0", "" },
            { "1", "one" },
            { "2", "two" },
            { "3", "three" },
            { "4", "four" },
            { "5", "five" },
            { "6", "six" },
            { "7", "seven" },
            { "8", "eight" },
            { "9", "nine" },
            { "10", "ten" },
            { "11", "eleven" },
            { "12", "twelve" },
            { "13", "thirteen" },
            { "14", "fourteen" },
            { "15", "fifteen" },
            { "16", "sixteen" },
            { "17", "seventeen" },
            { "18", "eighteen" },
            { "19", "nineteen" },
            { "20", "twenty" },
            { "30", "thirty" },
            { "40", "forty" },
            { "50", "fifty" },
            { "60", "sixty" },
            { "70", "seventy" },
            { "80", "eighty" },
            { "90", "ninety" },
        };

        static string ToWord(int i)
        {
            string value;
            var answer = new StringBuilder();
            string asString = i.ToString();
            
            if (intToWord.TryGetValue(asString, out value))
            {
                return value;
            }
            if (asString.Length == 4)
            {
                answer.Append("thousand");
                answer.Append(intToWord[asString[0].ToString()]);
                asString = asString.Substring(1);
                
                if (asString == "000")
                {
                    return answer.ToString();
                }
            }
            
            
            if (asString.Length == 3)
            {
                answer.Append("hundredand");
                answer.Append(intToWord[asString[0].ToString()]);
                asString = asString.Substring(1);
                
                if (asString == "00")
                {
                    answer.Remove("hundred".Length, 3);
                }
            }
            
            if (asString.Length == 2)
            {
                if (intToWord.TryGetValue(asString, out value))
                {
                    answer.Append(value);
                    return answer.ToString();
                }
                
                if (asString[0] != '0')
                {
                    answer.Append(intToWord[string.Format("{0}0", asString[0])]);
                }
                answer.Append(intToWord[asString[1].ToString()]);
            }
            
            
            return answer.ToString();
        }

        public override string ToString()
        {
            int n = 1000;
            long totalLength = 0;
            for (int i = 1; i <= n; i++)
            {
                var word = ToWord(i);
                totalLength += word.Length;
            }
            
            
            return totalLength.ToString();
        }
    }
}


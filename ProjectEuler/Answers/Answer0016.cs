﻿using System;
using System.Text;

namespace ProjectEuler
{
    /// <summary>
    /// 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
    /// What is the sum of the digits of the number 2^1000?
    /// </summary>
    public class Answer0016
    {
        public class Digit
        {
            public int Value
            {
                get;
                set;
            }

            public Digit Left
            {
                get;
                set;
            }

            public Digit Right
            {
                get;
                set;
            }
        }

        public class BigNumber
        {
            public BigNumber(int n = 1)
            {
                _TrailingDigit = new Digit() { Value = n };
                _LeadingDigit = TrailingDigit;
            }

            public void MultiplyBy(int n)
            {
                var digit = TrailingDigit;
                int carryOver = 0;
                while (digit != null)
                {
                    var product = digit.Value * n;
                    if (carryOver > 0)
                    {
                        product += carryOver;
                        carryOver = 0;
                    }
                    if (product > 9)
                    {
                        if (digit.Left == null)
                        {
                            var insertDigit = new Digit();
                            insertDigit.Left = digit.Left;
                            digit.Left = insertDigit;
                            insertDigit.Right = digit;
                            
                            if (insertDigit.Left == null)
                            {
                                _LeadingDigit = insertDigit;
                            }
                            else
                            {
                                insertDigit.Left.Right = insertDigit;
                            }
                        }                        
                        carryOver = product / 10;
                    }
                    digit.Value = product % 10;
                    digit = digit.Left;
                }
            }

            public int SumOfDigits()
            {
                var digit = LeadingDigit;
                int sum = 0;
                while (digit != null)
                {
                    sum += digit.Value;
                    digit = digit.Right;
                }
                return sum;
                
            }

            public override string ToString()
            {
                var sb = new StringBuilder();
                var digit = LeadingDigit;
                while (digit != null)
                {
                    sb.Append(digit.Value.ToString());
                    digit = digit.Right;
                }
                
                return sb.ToString();
                ;
            }

            private Digit _LeadingDigit;

            public Digit LeadingDigit
            {
                get
                {
                    return _LeadingDigit;
                }
            }

            private Digit _TrailingDigit;

            public Digit TrailingDigit
            {
                get
                {
                    return _TrailingDigit;
                }
            }
        }
        public override string ToString()
        {
            int n = 1000;
            var answer = new BigNumber();
            for (int i = 0; i < n; i++)
            {
                answer.MultiplyBy(2);
            }
            
            return answer.SumOfDigits().ToString();
        }
    }
}


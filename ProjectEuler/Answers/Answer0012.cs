﻿using System;
using System.Collections.Generic;

namespace ProjectEuler
{
    /// <summary>
    /// What is the value of the first triangle number to have over five hundred divisors?
    /// </summary>
    public class Answer0012
    {
        public static List<long> GetFactors(long n)
        {
            List<long> factors = new List<long>();
            for (long i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    factors.Add(i);
                }
            }
            return factors;
        }

        public override string ToString()
        {
            long triangleNumber = 0;
            int totalDivisors = 500;
            for (int i = 1; true; i++)
            {
                triangleNumber += i;
                
                if (GetFactors(triangleNumber).Count > totalDivisors)
                {
                    return triangleNumber.ToString();
                }
            }
        }
    }
}


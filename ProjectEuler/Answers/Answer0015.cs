﻿using System;

namespace ProjectEuler
{
    public class Answer0015
    {
        static ulong counter = 0;

        public static ulong SolutionA(int goalX = 20, int goalY = 20)
        {
            Traverse(0, 0, goalX, goalY);

            return counter;
        }

        public static void Traverse(int x, int y, int goalX, int goalY)
        {
            if (x == goalX && y == goalY)
            {
                counter++;
                return;
            }

            if (x + 1 <= goalX)
            {
                Traverse(x + 1, y, goalX, goalY);
            }
            
            if (y + 1 <= goalY)
            {
                Traverse(x, y + 1, goalX, goalY);
            }
        }

        public static ulong SolutionB(int goalX = 20, int goalY = 20)
        {
            ulong[,] grid = new ulong[goalX * 2 + 1, goalY * 2 + 1];
            for (int k = 0; grid[goalX, goalX] == 0; k++)
            {
                grid[k, 0] = 1;
                grid[0, k] = 1;
                for (int i = 0, j = k; i < k; i++, j--)
                {
                    if (i - 1 >= 0 && j - 1 >= 0)
                    {
                        grid[i, j] = grid[i, j - 1] + grid[i - 1, j];
                    }
                }
            }
            
            return grid[goalX, goalX];
        }

        public override string ToString()
        {
            return SolutionB().ToString();
        }
    }
}


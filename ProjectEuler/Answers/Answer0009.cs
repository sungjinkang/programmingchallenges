﻿using System;

namespace ProjectEuler
{
    /// <summary>
    /// There exists exactly one Pythagorean triplet for which a + b + c = 1000.
    /// Find the product abc.
    /// </summary>
    public class Answer0009
    {
        public override string ToString()
        {
            int targetProd = 1000;


            for (int a = 1; a <= targetProd; a++)
            {
                for (int b = a + 1; a + b <= targetProd; b++)
                {
                    for (int c = b + 1; a + b + c <= targetProd; c++)
                    {
                        if (a + b + c == targetProd && IsTriplet(a, b, c))
                        {
                            return (a * b * c).ToString();
                        }
                    }
                }
            }
            return (-1).ToString();
        }

        public static bool IsTriplet(int a, int b, int c)
        {
            return a < b &&
            b < c &&
            a * a + b * b == c * c;
        }

    }
}


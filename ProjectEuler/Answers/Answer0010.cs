﻿using System;
using System.Collections.Generic;

namespace ProjectEuler
{
    /// <summary>
    /// Find the sum of all the primes below two million.
    /// </summary>
    public class Answer0010
    {
        public override string ToString()
        {
            long n = 2000000L;
            long sum = 0L;
            var primes = Sieve(n);
            foreach (var prime in primes)
            {
                sum += prime;
            }
            return sum.ToString();
        }

        public static LinkedList<long> Sieve(long n)
        {
            LinkedList<long> primes = new LinkedList<long>();
            bool[] sieve = new bool[n < 10 ? 10 : n];
            int rootN = (int)Math.Sqrt(n);
            for (int i = 2; i < rootN; i++)
            {
                if (sieve[i])
                {
                    continue;
                }
                
                for (int j = 2 * i; j < n; j += i)
                {
                    sieve[j] = true;
                }
            }
            
            for (int i = 2; i < n; i++)
            {
                if (sieve[i] == false)
                {
                    primes.AddLast(i);
                }
            }
            
            
            return primes;
        }
    }
}


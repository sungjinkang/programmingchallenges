﻿using System;

namespace ProjectEuler
{
    /// <summary>
    /// What is the smallest positive number that is evenly divisible
    /// by all of the numbers from 1 to 20?
    /// </summary>
    public class Answer0005
    {
        public override string ToString()
        {
            var start = 1UL;
            var end = 20UL;
            ulong n = 2520;
            int count = 2;
            while (count == 2)
            {
                for (ulong i = start; i <= end; i++)
                {
                    if (n % i != 0)
                    {
                        count = 2;
                        n++;
                        break;
                    }

                    count++;
                }
                if (count > 2)
                {
                    break;
                }
            }
            return n.ToString();
        }
    }
}


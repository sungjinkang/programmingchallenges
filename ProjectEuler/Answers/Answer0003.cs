﻿using System;
using System.Collections.Generic;

namespace ProjectEuler
{
    /// <summary>
    /// What is the largest prime factor of the number 600851475143 ?
    /// </summary>
    public class Answer0003
    {
        public override string ToString()
        {
            var n = 600851475143L;
            var primes = PrimeSieve(n);
            var factors = FindFactors(primes, n);
            return factors.First.Value.ToString();
        }

        public static LinkedList<long> PrimeSieve(long n)
        {
            n = (long)Math.Sqrt(n);
            bool[] sieve = new bool[n];
            for (long i = 2; i < n; i++)
            {
                if (sieve[i])
                {
                    continue;
                }
                
                for (long j = i * 2; j < n; j += i)
                {
                    sieve[j] = true;
                }
            }
            
            var primes = new LinkedList<long>();
            for (long i = 2; i < n; i++)
            {
                if (sieve[i] == false)
                {
                    primes.AddLast(i);
                }
            }
            
            return primes;
        }

        public static LinkedList<long> FindPrimes(LinkedList<long> primes, long n)
        {
            for (long i = 7; i < n; i++)
            {
                var isPrime = true;
                foreach (var prime in primes)
                {
                    if (i % prime == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime)
                {
                    primes.AddFirst(i);
                }
            }
            return primes;  
        }

        public static LinkedList<long> FindFactors(LinkedList<long> primes, long n)
        {
            var factors = new LinkedList<long>();
            var prime = primes.Last;
            while (prime != null)
            {
                if (n % prime.Value == 0)
                {
                    factors.AddLast(prime.Value);
                }
                prime = prime.Previous;
            }
            return factors;
        }

    }
}


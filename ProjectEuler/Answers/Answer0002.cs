﻿using System;

namespace ProjectEuler
{
    /// <summary>
    /// By considering the terms in the Fibonacci sequence whose values do 
    /// not exceed four million, find the sum of the even-valued terms.
    /// </summary>
    public class Answer0002
    {
        public override string ToString()
        {
            var n = 4000000L;
            
            var sequence = new long[n];
            var sum = 0L;
            sequence[0] = 1L;
            sum += sequence[1] = 2L;
            sequence[2] = 3L;
            sequence[3] = 5L;
            
            
            for (long i = 4; i <= sequence.LongLength; i++)
            {
                var temp = sequence[i - 2] + sequence[i - 1];
                sequence[i] = temp;
                
                if (temp % 2 == 0)
                {
                    sum += sequence[i];
                }
                if (temp > n)
                {
                    return sum.ToString();
                }
            }
            
            return sum.ToString();
        }
    }
}


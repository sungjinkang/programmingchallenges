﻿using System;

namespace ProjectEuler
{
    public class Answer0019
    {
        public override string ToString()
        {
            return Solution().ToString();
        }

        object Solution()
        {
            var start = new DateTime(1901, 1, 1);
            var end = new DateTime(2000, 12, 31);

            var days = (end - start).Days;

            var found = 0;
            for (int i = 0; i <= days; i++)
            {
                var date = start.AddDays(i);
                if (date.DayOfWeek == DayOfWeek.Sunday && date.Day == 1)
                {
                    found++;
                }
            }
          
            return found;

        }
    }
}


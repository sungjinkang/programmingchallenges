﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace ProjectEuler
{
    public class Answer0018
    {
        Node topNode;
        public string Triangle = @"75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23";

        public override string ToString()
        {
            return Solution().ToString();
        }

        List<List<Node>> allNodes = new List<List<Node>>();

        public long Solution()
        {
            var lines = Triangle.Split('\n').Reverse().ToList();

            var count = lines.Count;

            Node lastNode;
            List<Node> previousNodeSet = null;
            for (int i = 0; i < count; i++)
            {
                var numbers = lines[i].Split(' ').Select(c => Convert.ToInt64(c)).ToList();
                var nodes = new List<Node>();
                var totalNumbers = numbers.Count;
                for (int j = 0; j < totalNumbers; j++)
                {
                    var number = numbers[j];
                    var node = new Node
                    {
                        Value = number,
                        BestPathSum = number,
                    };

                    nodes.Add(node);
                }

                if (previousNodeSet != null)
                {
                    nodes[0].Left = previousNodeSet[0];
                    previousNodeSet[0].RightParent = nodes[0];

                    nodes[0].Right = previousNodeSet[1];
                    previousNodeSet[1].LeftParent = nodes[0];

                    nodes[0].BestPathSum = previousNodeSet[0].BestPathSum + previousNodeSet[1].BestPathSum + nodes[0].Value;

                    for (int j = 1; j < nodes.Count; j++)
                    {
                        var node = nodes[j];
                        node.Left = previousNodeSet[j];
                        previousNodeSet[j].RightParent = node;

                        node.Right = previousNodeSet[j + 1];
                        previousNodeSet[j + 1].LeftParent = node;

                        nodes[j].BestPathSum = previousNodeSet[j].BestPathSum + previousNodeSet[j + 1].BestPathSum + nodes[j].Value;
                    }
                }
                previousNodeSet = nodes;

                allNodes.Add(nodes);
            }

            foreach (var row in allNodes)
            {
                foreach (var node in row)
                {
                    if (node.Left == null || node.Right == null)
                    {
                        node.BestPathSum = node.Value;
                        continue;
                    }

                    if (node.Left.BestPathSum > node.Right.BestPathSum)
                    {
                        node.BestPathSum = node.Value + node.Left.BestPathSum;
                    }
                    else //if (node.Left.Value < node.Right.Value)
                    {
                        node.BestPathSum = node.Value + node.Right.BestPathSum;
                    }
                }
            }
            var answer = allNodes.Last().Last().BestPathSum;
            return answer;
        }

        void Traverse(Node node)
        {

        }

        long SolveBackwards(Node node)
        {
            var leftPathSum = node.Left.BestPathSum + node.Left.Value;
            var rightPathSum = node.Right.BestPathSum + node.Right.Value;

            if (leftPathSum > rightPathSum)
            {
                return leftPathSum;
            }

            return rightPathSum;
        }


        List<long> totals = new List<long>();

        void BruteForce(Node node, long total)
        {
            if (node.Left == null || node.Right == null)
            {
                totals.Add(total);   
                return;
            }

            BruteForce(node.Left, total + node.Left.Value);
            BruteForce(node.Right, total + node.Right.Value);
        }


        public class Node
        {
            public Node LeftParent;
            public Node RightParent;

            public Node Left;
            public Node Right;


            public long Value;
            public long BestPathSum;

            public override string ToString()
            {
                return Value.ToString() + "(" + BestPathSum.ToString() + ")";
            }
        }
    }
}


﻿using System;

namespace ProjectEuler
{
    /// <summary>
    /// What is the 10 001st prime number?
    /// </summary>
    public class Answer0007
    {
        public override string ToString()
        {
            long target = 10001;
            long n = 2;
            for (long i = 0, j = n; i < target; j++)
            {
                if (IsPrime(j))
                {
                    i++;
                    n = j;
                }
            }
            return n.ToString();
        }

        public static bool IsPrime(long j)
        {
            if (j == 2)
            {
                return true;
            }
            if (j % 2 == 0)
            {
                return false;
            }
            
            for (long i = 2; i < j; i++)
            {
                if (j % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}


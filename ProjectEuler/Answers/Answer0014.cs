﻿using System;
using System.Collections.Generic;

namespace ProjectEuler
{
    public class Answer0014
    {
        public override string ToString()
        {
            return SolutionB(1000000).ToString();
        }

        public static long SolutionA(long length = 1000000)
        {
            int longestChainCount = 0;
            long result = 0;
            
            for (long i = 2; i < length; i++)
            {
                int count = 1;
                long j = i;
                while (j > 1)
                {
                    j = (j % 2 == 0) ? (j / 2) : (3 * j + 1);
                    count++;
                }
                if (count >= longestChainCount)
                {
                    longestChainCount = count;
                    result = i;
                }
                
            }

            return result;
        }

        public static long SolutionB(long length = 1000000)
        {
            Dictionary<long, int> chainCounts = new Dictionary<long, int>();
            long longestChainCount = 0;
            long result = 0;
            
            for (long i = 1; i < length; i++)
            {
                chainCounts[i] = 1;
                long j = i;
                long temp = j;
                while (j > 1)
                {
                    j = (j % 2 == 0) ? (j / 2) : (3 * j + 1);
                    if (chainCounts.ContainsKey(j))
                    {
                        chainCounts[i] += chainCounts[j];
                        break;
                    }
                    chainCounts[i]++;
                }
                if (chainCounts[i] >= longestChainCount)
                {
                    longestChainCount = chainCounts[i];
                    result = i;
                }
                
            }
            
            
            return result;
        }
    }
}


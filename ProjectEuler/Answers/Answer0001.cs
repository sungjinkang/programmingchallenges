﻿using System;


namespace ProjectEuler
{
    /// <summary>
    /// Find the sum of all the multiples of 3 or 5 below 1000.
    /// </summary>
    public class Answer0001
    {
        public override string ToString()
        {
            var n = 1000;
            long sum = 0;
            for (long i = 0; i < n; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    sum += i;
                }
            }
            return sum.ToString();
        }
    }
}


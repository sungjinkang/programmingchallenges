﻿using System;

namespace ProjectEuler
{
    public class Answer0020
    {
        public override string ToString()
        {
            return Solution().ToString();
        }

        object Solution()
        {
            var bigNumber = new Answer0016.BigNumber();
            for (int i = 2; i < 100; i++)
            {
                bigNumber.MultiplyBy(i);
            }

            var curr = bigNumber.LeadingDigit;
            var sum = 0;
            while (curr != null)
            {
                sum += curr.Value;
                curr = curr.Right;
            }
            return sum;
        }

    }
}


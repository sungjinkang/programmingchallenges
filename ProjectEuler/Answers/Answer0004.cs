﻿using System;

namespace ProjectEuler
{
    /// <summary>
    /// Find the largest palindrome made from the product of two 3-digit numbers.
    /// </summary>
    public class Answer0004
    {
        public override string ToString()
        {
            int size = 3;
            int max = 9;
            if (size < 1)
            {
                throw new Exception("something about things not wokring");
            }
            for (int i = 1; i < size; i++)
            {
                max += (int)(9 * Math.Pow(10, i));
            }
            
            
            int a = 1;
            int b = 1;
            int largest = int.MinValue;
            
            while (b < 999)
            {
                var product = a * b;
                if (IsPalandrome(product) && product > largest)
                {
                    largest = product;
                }
                
                a++;
                if (a > 999)
                {
                    a = 1;
                    b++;
                }
            }
            return largest.ToString();
        }

        public static bool IsPalandrome(int value)
        {
            string s = value.ToString();
            for (int i = 0; i < s.Length / 2; i++)
            {
                if (s[i] != s[s.Length - i - 1])
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}

